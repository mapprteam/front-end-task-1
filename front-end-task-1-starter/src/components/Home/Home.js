import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../logo.svg';
import './Home.css';

function Home (props) {
  return (
    <div>
      <img src={logo} className="Home-logo" alt="logo"/>
      <p>
        Edit <code>src/components/Home.js</code> and save to reload.
      </p>
      <a
        className="Home-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
      >
        Learn React
      </a>
    </div>
  );
}

Home.propTypes = {};
Home.defaultProps = {};

export default Home;
